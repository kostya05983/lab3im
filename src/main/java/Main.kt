import javafx.application.Application
import javafx.scene.Scene
import javafx.scene.chart.LineChart
import javafx.scene.chart.NumberAxis
import javafx.scene.chart.XYChart
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.layout.AnchorPane
import javafx.stage.Stage
import javafx.collections.FXCollections
import javafx.collections.ObservableList
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.input.MouseEvent
import javafx.scene.layout.AnchorPane.*
import javafx.scene.shape.Rectangle
import java.util.*


class Main : Application() {
    private var _anchorPane: AnchorPane? = null
    private var _levelsLineChart: LineChart<Number, Number>? = null
    private var _tempsLineChart: LineChart<Number, Number>? = null
    private var _textFieldsList: List<TextField>? = null
    private var _fieldOutput: TextArea? = null
    private var _fieldReplenishmentA: TextArea? = null
    private var _fieldReplenishmentB: TextArea? = null
    private var _fieldAmountDetails: TextField? = null
    private var _fieldTime: TextField? = null
    private var _mainStage: Stage? = null
    private var _mainScene: Scene? = null


    override fun start(primaryStage: Stage?) {
        _anchorPane = AnchorPane()
        _anchorPane?.setMinSize(1300.0, 850.0)
        _anchorPane?.setMaxSize(1300.0, 850.0)

        initLabels()
        initFields()
        initFunc()
        initDefaultValues()

        val button = Button("OK")
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, ModelHandler())
        setLeftAnchor(button, 550.0)
        setTopAnchor(button, 400.0)
        _anchorPane?.children?.add(button)

        _mainScene = Scene(_anchorPane)

        _mainStage = primaryStage
        primaryStage?.isFullScreen = false
        primaryStage?.scene = _mainScene
        primaryStage?.maxWidth = 1300.0



        primaryStage?.show()
    }

    private fun initLabels() {
        val labelsList = listOf(
                Label("Темпы"),
                Label("Уровень заготовок деталей А на складе"),
                Label("Уровень заготовок деталей B на складе"),
                Label("Уровни"),
                Label("Уровень деталей А в сб отделе"),
                Label("Уровенье деталей B в сб отделе"),
                Label("Па"),
                Label("Пб"),
                Label("Дmax"),
                Label("Дср"),
                Label("Дmin"),
                Label("deltaMu"),
                Label("Alpha"),
                Label("deltaT")
        )
        val labelReplenishmentA = Label()
        val labelReplenishmentB = Label()
        val labelAmountDetails = Label()
        val labelTime = Label()

        for (i in 0 until labelsList.size) {
            setTopAnchor(labelsList[i], i * 30.0 + 20)
            setLeftAnchor(labelsList[i], 25.0)
            _anchorPane?.children?.add(labelsList[i])
        }


        setTopAnchor(labelReplenishmentA, 20.0)
        setLeftAnchor(labelReplenishmentA, 440.0)
        labelReplenishmentA.text = "Пополнение А"
        _anchorPane?.children?.add(labelReplenishmentA)

        setTopAnchor(labelReplenishmentB, 20.0)
        setLeftAnchor(labelReplenishmentB, 650.0)
        labelReplenishmentB.text = "Пополнение B"
        _anchorPane?.children?.add(labelReplenishmentB)

        setTopAnchor(labelAmountDetails, 350.0)
        setLeftAnchor(labelAmountDetails, 500.0)
        labelAmountDetails.text = "Количество деталей"
        _anchorPane?.children?.add(labelAmountDetails)

        setTopAnchor(labelTime, 350.0)
        setLeftAnchor(labelTime, 410.0)
        labelTime.text = "Время"
        _anchorPane?.children?.add(labelTime)

        _anchorPane?.setMinSize(750.0, 500.0)

    }

    private fun initFields() {
        _textFieldsList = listOf(
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField(),
                TextField()
        )
        _fieldAmountDetails = TextField()
        _fieldReplenishmentA = TextArea()
        _fieldReplenishmentB = TextArea()
        _fieldOutput = TextArea()
        _fieldTime = TextField()

        for (i in 0 until _textFieldsList!!.size) {
            _textFieldsList!![i].setMaxSize(60.0, 15.0)
            setTopAnchor(_textFieldsList!![i], i * 30.0 + 20)
            setLeftAnchor(_textFieldsList!![i], 320.0)
            _anchorPane?.children?.add(_textFieldsList!![i])
        }

        _fieldAmountDetails!!.setMaxSize(60.0, 15.0)
        setTopAnchor(_fieldAmountDetails, 370.0)
        setLeftAnchor(_fieldAmountDetails, 540.0)
        _anchorPane?.children?.add(_fieldAmountDetails)

        _fieldTime!!.setMaxSize(60.0, 15.0)
        setTopAnchor(_fieldTime, 370.0)
        setLeftAnchor(_fieldTime, 400.0)
        _anchorPane?.children?.add(_fieldTime)

        _fieldReplenishmentA!!.setMinSize(180.0, 300.0)
        _fieldReplenishmentA!!.setMaxSize(180.0, 300.0)
        setTopAnchor(_fieldReplenishmentA, 40.0)
        setLeftAnchor(_fieldReplenishmentA, 400.0)
        _anchorPane?.children?.add(_fieldReplenishmentA)

        _fieldReplenishmentB!!.setMinSize(180.0, 300.0)
        _fieldReplenishmentB!!.setMaxSize(180.0, 300.0)
        setTopAnchor(_fieldReplenishmentB, 40.0)
        setLeftAnchor(_fieldReplenishmentB, 610.0)
        _anchorPane?.children?.add(_fieldReplenishmentB)

        _fieldOutput!!.setMinSize(250.0, 750.0)
        _fieldOutput!!.setMaxSize(250.0, 750.0)
        setTopAnchor(_fieldOutput, 40.0)
        setLeftAnchor(_fieldOutput, 800.0)
        setRightAnchor(_fieldOutput, 10.0)
        _anchorPane?.children?.add(_fieldOutput)

    }

    private fun initFunc() {
        val x = NumberAxis()
        val y = NumberAxis()
        val x1 = NumberAxis()
        val y1 = NumberAxis()

        _levelsLineChart = LineChart<Number, Number>(x, y)
        _levelsLineChart?.title = "Уровни"
        setTopAnchor(_levelsLineChart, 430.0)
        _levelsLineChart?.setMaxSize(400.0, 400.0)
        _anchorPane?.children?.add(_levelsLineChart)

        _tempsLineChart = LineChart<Number, Number>(x1, y1)
        _tempsLineChart?.title = "Темпы"
        setTopAnchor(_tempsLineChart, 430.0)
        setLeftAnchor(_tempsLineChart, 400.0)
        _tempsLineChart?.setMaxSize(400.0, 400.0)
        _anchorPane?.children?.add(_tempsLineChart)


    }

    private fun initDefaultValues() {
        _textFieldsList!![0].text = "10"
        _textFieldsList!![1].text = "150"
        _textFieldsList!![2].text = "120"
        _textFieldsList!![3].text = "30"
        _textFieldsList!![4].text = "25"
        _textFieldsList!![5].text = "50"
        _textFieldsList!![6].text = "50"
        _textFieldsList!![7].text = "100"
        _textFieldsList!![8].text = "12"
        _textFieldsList!![9].text = "8"
        _textFieldsList!![10].text = "4"
        _textFieldsList!![11].text = "0.05"
        _textFieldsList!![12].text = "1"
        _textFieldsList!![13].text = "1"
        _fieldTime?.text = "100"
    }

    inner class ModelHandler : EventHandler<MouseEvent> {

        override fun handle(event: MouseEvent) {
            _levelsLineChart?.data?.clear()
            _tempsLineChart?.data?.clear()
            _fieldOutput?.clear()
            _fieldReplenishmentA?.clear()
            _fieldReplenishmentB?.clear()

            var t = 0
            val timeMax = _fieldTime?.text?.toDouble()!!
            val criticalLevel = 0.2
            var amountDetails = 0
            val complexityA: Double = _textFieldsList?.get(6)?.text?.toDouble()!!
            val complexityB: Double = _textFieldsList?.get(7)?.text?.toDouble()!!
            var levelDetailsDepartmentA: Double = _textFieldsList?.get(4)?.text?.toDouble()!!
            var levelDetailsDepartmentB: Double = _textFieldsList?.get(5)?.text?.toDouble()!!
            val levelDetailsStockA: Double = _textFieldsList?.get(1)?.text?.toDouble()!!
            val levelDetailsStockB: Double = _textFieldsList?.get(2)?.text?.toDouble()!!
            val alpha: Double = _textFieldsList?.get(12)?.text?.toDouble()!!
            val deltaMu: Double = _textFieldsList?.get(11)?.text?.toDouble()!!
            val dMin: Double = _textFieldsList?.get(10)?.text?.toDouble()!!
            val dAverage: Double = _textFieldsList?.get(9)?.text?.toDouble()!!
            val dMax: Double = _textFieldsList?.get(8)?.text?.toDouble()!!
            val deltaT: Double = _textFieldsList?.get(13)?.text?.toDouble()!!
            val temps: Array<Double> = Array(7, { _textFieldsList?.get(3)?.text?.toDouble()!! })
            val levels: Array<Double> = Array(5, { _textFieldsList?.get(0)?.text?.toDouble()!! })
            val delays: Array<Double> = Array(7, { dAverage })
            var x1 = levelDetailsStockA
            var x2 = levelDetailsStockB

            val levelSeries: Array<XYChart.Series<Number, Number>> = Array(5, { XYChart.Series<Number, Number>() })
            val levelData: Array<ObservableList<XYChart.Data<Number, Number>>> = Array(5, { FXCollections.observableArrayList<XYChart.Data<Number, Number>>() })
            val tempsSeries: Array<XYChart.Series<Number, Number>> = Array(7, { XYChart.Series<Number, Number>() })
            val tempsData: Array<ObservableList<XYChart.Data<Number, Number>>> = Array(7, { FXCollections.observableArrayList<XYChart.Data<Number, Number>>() })


            while (t < timeMax) {
                _fieldOutput?.text = _fieldOutput?.text + "Время $t\n"
                x1 -= temps[0]
                x2 -= temps[4]

                levels[0] += deltaT * (temps[0] - temps[1])
                levels[1] += deltaT * (temps[1] - temps[2])
                levels[2] += deltaT * (temps[2] - temps[3])
                levels[3] += deltaT * (temps[4] - temps[5])
                levels[4] += deltaT * (temps[5] - temps[6])

                for (i in 0 until levelData.size)
                    levelData[i].add(XYChart.Data(t, levels[i]))

                _fieldOutput?.text = _fieldOutput?.text + "y11=" + levels[0] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "y12=" + levels[1] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "y13=" + levels[2] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "y21=" + levels[3] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "y22=" + levels[4] + "\n"

                levelDetailsDepartmentA += temps[3]
                levelDetailsDepartmentB += temps[6]

                _fieldOutput?.text = _fieldOutput?.text + "A=$levelDetailsDepartmentA B=$levelDetailsDepartmentB\n"

                while (mu(levelDetailsDepartmentA, complexityA) >= 1.0 && mu(levelDetailsDepartmentB, complexityB) >= 1.0) {
                    levelDetailsDepartmentA -= complexityA
                    levelDetailsDepartmentB -= complexityB
                    amountDetails++
                }

                _fieldOutput?.text = _fieldOutput?.text + "Колличество изготовленных деталей=$amountDetails\n"

                if (Math.abs(mu(levelDetailsDepartmentA, complexityA) - mu(levelDetailsDepartmentB, complexityB)) > deltaMu) {
                    if (mu(levelDetailsDepartmentA, complexityA) > mu(levelDetailsDepartmentB, complexityB)) {
                        delays[0] = (1 - 1 / Math.abs(dMin + (Math.floor((dAverage * x1) / delays[0]) + alpha * dMax))) * (dMax - dMin) + dMin
                        delays[1] = (1 - 1 / Math.abs(dMin + (Math.floor((dAverage * levels[0]) / delays[1]) + alpha * dMax))) * (dMax - dMin) + dMin
                        delays[2] = (1 - 1 / Math.abs(dMin + (Math.floor((dAverage * levels[1]) / delays[2]) + alpha * dMax))) * (dMax - dMin) + dMin
                        delays[3] = (1 - 1 / Math.abs(dMin + (Math.floor((dAverage * levels[2]) / delays[3]) + alpha * dMax))) * (dMax - dMin) + dMin
                    } else {
                        delays[4] = (1 - 1 / Math.abs(dMin + (Math.floor((dAverage * x2) / delays[4]) + alpha * dMax))) * (dMax - dMin) + dMin
                        delays[5] = (1 - 1 / Math.abs(dMin + (Math.floor((dAverage * levels[3]) / delays[5]) + alpha * dMax))) * (dMax - dMin) + dMin
                        delays[6] = (1 - 1 / Math.abs(dMin + (Math.floor((dAverage * levels[4]) / delays[6]) + alpha * dMax))) * (dMax - dMin) + dMin
                    }
                }

                _fieldOutput?.text = _fieldOutput?.text + "d10=" + delays[0] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "d11=" + delays[1] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "d12=" + delays[2] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "d13=" + delays[3] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "d20=" + delays[4] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "d21=" + delays[5] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "d22=" + delays[6] + "\n"

                temps[0] = Math.floor(x1 / delays[0])
                temps[1] = Math.floor(levels[0] / delays[1])
                temps[2] = Math.floor(levels[1] / delays[2])
                temps[3] = Math.floor(levels[2] / delays[3])
                temps[4] = Math.floor(x2 / delays[4])
                temps[5] = Math.floor(levels[3] / delays[5])
                temps[6] = Math.floor(levels[4] / delays[6])

                for (i in 0 until tempsData.size)
                    tempsData[i].add(XYChart.Data(t, temps[i]))

                _fieldOutput?.text = _fieldOutput?.text + "x10=" + temps[0] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "x11=" + temps[1] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "x12=" + temps[2] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "x13=" + temps[3] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "x20=" + temps[4] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "x21=" + temps[5] + "\n"
                _fieldOutput?.text = _fieldOutput?.text + "x22=" + temps[6] + "\n"


                if (criticalLevel * levelDetailsStockA > x1) {
                    if (x1 <= 0.05 * levelDetailsStockA) {
                        _fieldOutput?.text = "Нехватка\n"
                        break
                    }

                    val tmp = getNumber()

                    if ((tmp >= 0.0) && (tmp <= 0.4)) {
                        x1 += Math.floor(0.1 * levelDetailsStockA)
                    }

                    if ((tmp >= 0.4) && (tmp <= 0.7)) {
                        x1 += Math.floor(0.2 * levelDetailsStockA)
                    }

                    if ((tmp >= 0.7) && (tmp <= 0.9)) {
                        x1 += Math.floor(0.5 * levelDetailsStockA)
                    }

                    if ((tmp >= 0.9) && (tmp <= 1.0)) {
                        x1 += Math.floor(1.0 * levelDetailsStockA)
                    }

                    _fieldReplenishmentA?.text = _fieldReplenishmentA?.text + "T=$t вероятность пополнения=$tmp x1=$x1\n"
                }

                if (criticalLevel * levelDetailsStockB > x2) {
                    if (x2 <= 0.05 * levelDetailsStockB) {
                        _fieldOutput?.text = "Нехватка\n"
                        break
                    }

                    val tmp = getNumber()

                    if ((tmp >= 0.0) && (tmp <= 0.4)) {
                        x2 += Math.floor(0.1 * levelDetailsStockB)
                    }

                    if ((tmp >= 0.4) && (tmp <= 0.7)) {
                        x2 += Math.floor(0.2 * levelDetailsStockB)
                    }

                    if ((tmp >= 0.7) && (tmp <= 0.9)) {
                        x2 += Math.floor(0.5 * levelDetailsStockB)
                    }

                    if ((tmp >= 0.9) && (tmp <= 1.0)) {
                        x2 += Math.floor(1.0 * levelDetailsStockB)
                    }

                    _fieldReplenishmentB?.text = _fieldReplenishmentB?.text + "T=$t вероятность пополнения=$tmp x2$x2\n"
                }
                t++
            }

            _fieldAmountDetails?.text = "$amountDetails"

            for (i in 0 until levelSeries.size) {
                levelSeries[i].data = levelData[i]
                for (j in 0 until levelSeries[i].data.size) {
                    val rect = Rectangle(0.0, 0.0)
                    rect.isVisible = false
                    levelSeries[i].data[j].node = rect
                }
                _levelsLineChart?.data?.add(levelSeries[i])
            }

            levelSeries[0].name = "y11"
            levelSeries[1].name = "y12"
            levelSeries[2].name = "y13"
            levelSeries[3].name = "y21"
            levelSeries[4].name = "y22"

            for (i in 0 until tempsSeries.size) {
                tempsSeries[i].chart?.isLegendVisible = false
                tempsSeries[i].data = tempsData[i]
                for (j in 0 until tempsSeries[i].data.size) {
                    val rect = Rectangle(0.0, 0.0)
                    rect.isVisible = false
                    tempsSeries[i].data[j].node = rect
                }
                _tempsLineChart?.data?.add(tempsSeries[i])
            }

            tempsSeries[0].name = "x10"
            tempsSeries[1].name = "x11"
            tempsSeries[2].name = "x12"
            tempsSeries[3].name = "x13"
            tempsSeries[4].name = "x20"
            tempsSeries[5].name = "x21"
            tempsSeries[6].name = "x22"


            _levelsLineChart?.addEventHandler(MouseEvent.MOUSE_CLICKED, GraphicHandler(getCopy(_levelsLineChart), _mainScene, _mainStage))
            _tempsLineChart?.addEventHandler(MouseEvent.MOUSE_CLICKED, GraphicHandler(getCopy(_tempsLineChart), _mainScene, _mainStage))
        }

        private fun mu(levelDetailsDepartment: Double, complexity: Double): Double {
            return levelDetailsDepartment / complexity
        }

        private fun getNumber(): Double {
            val random = Random()

            return random.nextDouble()
        }

        private fun getCopy(original: LineChart<Number, Number>?): LineChart<Number, Number>? {
            val x = NumberAxis()
            val y = NumberAxis()
            val result: LineChart<Number, Number> = LineChart<Number, Number>(x, y)

            for (i in 0 until original!!.data.size) {
                val series: XYChart.Series<Number, Number> = XYChart.Series()
                val data = FXCollections.observableArrayList<XYChart.Data<Number, Number>>(original.data[i].data)
                series.data = data
                series.name = "" + original.data[i].name
                result.data.add(series)
            }



            return result
        }

    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            launch(Main::class.java)
        }
    }


}