import javafx.event.EventHandler
import javafx.geometry.Orientation
import javafx.scene.Scene
import javafx.scene.chart.LineChart
import javafx.scene.input.MouseEvent
import javafx.scene.layout.FlowPane
import javafx.stage.Stage

class GraphicHandler(private var _lineChart: LineChart<Number, Number>? = null, private val _mainScene: Scene? = null, private val _stage: Stage? = null) : EventHandler<MouseEvent> {
    private var _isBig = false

    override fun handle(event: MouseEvent?) {
        val flowPane = FlowPane(Orientation.HORIZONTAL)
        if (!_isBig) {
            val scene = Scene(flowPane)

            _lineChart!!.setMinSize(1000.0, 800.0)
            _lineChart!!.addEventHandler(MouseEvent.MOUSE_CLICKED, this)
            flowPane.children.add(_lineChart)

            _stage?.scene = scene
            _isBig = true
        } else {
            _stage?.scene = _mainScene
            _isBig = false
        }
    }


}